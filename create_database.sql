DROP TABLE IF EXISTS `business_role_privilege`;
DROP TABLE IF EXISTS `group_member_business_role`;
DROP TABLE IF EXISTS `group_invite`;
DROP TABLE IF EXISTS `business_role`;
DROP TABLE IF EXISTS `business_privilege`;
DROP TABLE IF EXISTS `test_completion`;
DROP TABLE IF EXISTS `student_test`;
DROP TABLE IF EXISTS `message`;
DROP TABLE IF EXISTS `group_member`;
DROP TABLE IF EXISTS `group`;
DROP TABLE IF EXISTS `user_security_role`;
DROP TABLE IF EXISTS `security_role`;
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`
(
    `user_id`          INT                                                     NOT NULL AUTO_INCREMENT,
    `username`         VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci  NOT NULL,
    `password`         VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    `email`            VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci  NOT NULL,
    `first_name`       VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci  NOT NULL,
    `last_name`        VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci  NOT NULL,
    `middle_name`      VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci           DEFAULT '',
    `logout_next_time` BOOLEAN                                                 NOT NULL DEFAULT FALSE,
    `banned`           BOOLEAN                                                 NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`user_id`)
);

CREATE TABLE `security_role`
(
    `security_role_id` INT                                                    NOT NULL AUTO_INCREMENT,
    `role_name`        VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`security_role_id`)
);

CREATE TABLE `user_security_role`
(
    `user_security_role_id` INT NOT NULL AUTO_INCREMENT,
    `user_id`               INT NOT NULL,
    `security_role_id`      INT NOT NULL,
    PRIMARY KEY (`user_security_role_id`),
    FOREIGN KEY (`user_id`) REFERENCES user (`user_id`),
    FOREIGN KEY (`security_role_id`) REFERENCES security_role (`security_role_id`)
);

CREATE TABLE `group`
(
    `group_id`           INT                                                    NOT NULL AUTO_INCREMENT,
    `group_name`         VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    `created_at`         DATETIME                                               NOT NULL DEFAULT NOW(),
    `created_by_user_id` INT                                                    NOT NULL,
    `business_id`        VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    `picture_url`        VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
    PRIMARY KEY (`group_id`),
    FOREIGN KEY (`created_by_user_id`) REFERENCES user (`user_id`)
);

CREATE TABLE `group_member`
(
    `group_member_id` INT      NOT NULL AUTO_INCREMENT,
    `group_id`        INT      NOT NULL,
    `user_id`         INT      NOT NULL,
    `join_date`       DATETIME NOT NULL DEFAULT NOW(),
    PRIMARY KEY (`group_member_id`),
    FOREIGN KEY (`group_id`) REFERENCES `group` (`group_id`),
    FOREIGN KEY (`user_id`) REFERENCES user (`user_id`)
);

CREATE TABLE `message`
(
    `message_id` INT                                                     NOT NULL AUTO_INCREMENT,
    `sent_at`    DATETIME                                                NOT NULL DEFAULT NOW(),
    `sent_by`    INT                                                     NOT NULL,
    `is_private` BOOLEAN                                                 NOT NULL,
    `sent_to`    INT,
    `message_text`    VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`message_id`),
    FOREIGN KEY (`sent_by`) REFERENCES `group_member` (`group_member_id`),
    FOREIGN KEY (`sent_to`) REFERENCES `group_member` (`group_member_id`)
);

CREATE TABLE `business_role`
(
    `business_role_id` INT                                                    NOT NULL AUTO_INCREMENT,
    `role_name`        VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`business_role_id`)
);

CREATE TABLE `group_invite`
(
    `group_invite_id`    INT                                                    NOT NULL AUTO_INCREMENT,
    `business_id`        VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    `used`               BOOLEAN                                                NOT NULL DEFAULT FALSE,
    `created_by_user_id` INT                                                    NOT NULL,
    `created_at`         DATETIME                                               NOT NULL DEFAULT NOW(),
    `to_group_id`        INT                                                    NOT NULL,
    `for_role_id`        INT                                                    NOT NULL,
    PRIMARY KEY (`group_invite_id`),
    FOREIGN KEY (`created_by_user_id`) REFERENCES user (`user_id`),
    FOREIGN KEY (`to_group_id`) REFERENCES `group` (`group_id`),
    FOREIGN KEY (`for_role_id`) REFERENCES business_role (`business_role_id`)
);

CREATE TABLE `business_privilege`
(
    `business_privilege_id` INT                                                    NOT NULL AUTO_INCREMENT,
    `privilege_name`        VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`business_privilege_id`)
);

CREATE TABLE `business_role_privilege`
(
    `business_role_privilege_id` INT NOT NULL AUTO_INCREMENT,
    `business_role_id`           INT NOT NULL,
    `business_privilege_id`      INT NOT NULL,
    PRIMARY KEY (`business_role_privilege_id`),
    FOREIGN KEY (`business_role_id`) REFERENCES business_role (`business_role_id`),
    FOREIGN KEY (`business_privilege_id`) REFERENCES business_privilege (`business_privilege_id`)
);

CREATE TABLE `group_member_business_role`
(
    `group_member_business_role_id` INT                                                    NOT NULL AUTO_INCREMENT,
    `business_role_id`              INT                                                    NOT NULL,
    `group_member_id`               INT                                                    NOT NULL,
    `role_nickname`                 VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`group_member_business_role_id`),
    FOREIGN KEY (`business_role_id`) REFERENCES business_role (`business_role_id`),
    FOREIGN KEY (`group_member_id`) REFERENCES group_member (`group_member_id`)
);

CREATE TABLE `student_test`
(
    `student_test_id`  INT                                                    NOT NULL AUTO_INCREMENT,
    `test_name`        VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    `parseable_string` TEXT                                                   NOT NULL,
    `created_at`       DATETIME                                               NOT NULL DEFAULT NOW(),
    `deadline`         DATETIME                                               NOT NULL,
    `hard_deadline`    BOOLEAN                                                NOT NULL DEFAULT TRUE,
    `created_by`       INT                                                    NOT NULL,
    `business_id`      VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    `available`        BOOLEAN                                                NOT NULL DEFAULT TRUE,
    PRIMARY KEY (`student_test_id`),
    FOREIGN KEY (`created_by`) REFERENCES group_member (`group_member_id`)
);

CREATE TABLE `test_completion`
(
    `test_completion_id` INT                                                    NOT NULL AUTO_INCREMENT,
    `test_completed_id`  INT                                                    NOT NULL,
    `completed_at`       DATETIME                                               NOT NULL DEFAULT NOW(),
    `completed_by`       INT                                                    NOT NULL,
    `result`             INT                                                    NOT NULL,
    `teacher_comment`    VARCHAR(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
    `solution_string`    TEXT                                                   NOT NULL,
    `business_id`        VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`test_completion_id`),
    FOREIGN KEY (`completed_by`) REFERENCES group_member (`group_member_id`),
    FOREIGN KEY (`test_completed_id`) REFERENCES student_test (`student_test_id`)
);



INSERT INTO security_role(`role_name`)
VALUES ('ROLE_ADMIN');
INSERT INTO security_role(`role_name`)
VALUES ('ROLE_USER');
INSERT INTO security_role(`role_name`)
VALUES ('ROLE_GUEST');


INSERT INTO business_privilege(`privilege_name`)
VALUES ('CREATE_TEST');
INSERT INTO business_privilege(`privilege_name`)
VALUES ('EDIT_TEST');
INSERT INTO business_privilege(`privilege_name`)
VALUES ('RETRIEVE_TEST_ALL');
INSERT INTO business_privilege(`privilege_name`)
VALUES ('RETRIEVE_TEST_ACTIVE');
INSERT INTO business_privilege(`privilege_name`)
VALUES ('COMPLETE_TEST');
INSERT INTO business_privilege(`privilege_name`)
VALUES ('READ_TEST_SCORE_ALL');
INSERT INTO business_privilege(`privilege_name`)
VALUES ('READ_TEST_SCORE_OWN');
INSERT INTO business_privilege(`privilege_name`)
VALUES ('READ_TEST_COMMENT_ALL');
INSERT INTO business_privilege(`privilege_name`)
VALUES ('READ_TEST_COMMENT_OWN');
INSERT INTO business_privilege(`privilege_name`)
VALUES ('ADD_TEST_COMMENT');
INSERT INTO business_privilege(`privilege_name`)
VALUES ('ADD_TEST_SCORE');
INSERT INTO business_privilege(`privilege_name`)
VALUES ('SEND_MESSAGE_ALL');
INSERT INTO business_privilege(`privilege_name`)
VALUES ('SEND_MESSAGE_PRIVATE');
INSERT INTO business_privilege(`privilege_name`)
VALUES ('READ_MESSAGE_PRIVATE_ALL');
INSERT INTO business_privilege(`privilege_name`)
VALUES ('INVITE_MEMBERS');

INSERT INTO business_role(`role_name`)
VALUES ('TEACHER');
INSERT INTO business_role(`role_name`)
VALUES ('ASSISTANT');
INSERT INTO business_role(`role_name`)
VALUES ('STUDENT');
INSERT INTO business_role(`role_name`)
VALUES ('GUEST');

INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 1);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 2);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 3);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 4);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 5);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 6);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 7);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 8);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 9);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 10);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 11);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 12);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 13);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 14);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (1, 15);

INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (2, 2);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (2, 3);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (2, 4);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (2, 5);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (2, 7);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (2, 9);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (2, 10);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (2, 11);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (2, 12);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (2, 13);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (2, 15);

INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (3, 4);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (3, 5);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (3, 7);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (3, 9);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (3, 12);

INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (4, 4);
INSERT INTO business_role_privilege(`business_role_id`, `business_privilege_id`)
VALUES (4, 12);
DROP TABLE IF EXISTS `message`;

CREATE TABLE `message`
(
    `message_id` INT                                                     NOT NULL AUTO_INCREMENT,
    `sent_at`    DATETIME                                                NOT NULL DEFAULT NOW(),
    `sent_by`    INT                                                     NOT NULL,
    `is_private` BOOLEAN                                                 NOT NULL,
    `sent_to`    INT,
    `message_text`    VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`message_id`),
    FOREIGN KEY (`sent_by`) REFERENCES `group_member` (`group_member_id`),
    FOREIGN KEY (`sent_to`) REFERENCES `group_member` (`group_member_id`)
);
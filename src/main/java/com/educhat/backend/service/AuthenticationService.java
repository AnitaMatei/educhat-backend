package com.educhat.backend.service;

import com.educhat.backend.datamodel.dto.UserDTO;
import com.educhat.backend.datamodel.entity.User;
import com.educhat.backend.datamodel.request.LoginRequest;
import com.educhat.backend.datamodel.request.LoginResponse;
import com.educhat.backend.datamodel.request.RegisterRequest;
import com.educhat.backend.exception.AlreadyExistingAccountException;
import com.educhat.backend.exception.InvalidLoginDetailsException;
import com.educhat.backend.exception.InvalidRegisterDetailsException;
import com.educhat.backend.repository.UserRepository;
import com.educhat.backend.security.SecurityHelper;
import com.educhat.backend.security.jwt.JwtTokenGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
@Slf4j
public class AuthenticationService {
    UserRepository userRepository;
    UserService userService;
    BCryptPasswordEncoder bCryptPasswordEncoder;
    JwtTokenGenerator jwtTokenGenerator;

    public AuthenticationService(UserRepository userRepository, UserService userService,
                                 BCryptPasswordEncoder bCryptPasswordEncoder, JwtTokenGenerator jwtTokenGenerator) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.jwtTokenGenerator = jwtTokenGenerator;
    }

    public LoginResponse loginWithUsernameAndPassword(LoginRequest loginRequest) {
        User user =
                userRepository.findByUsername(loginRequest.getUsername()).orElseThrow(InvalidLoginDetailsException::new);
        if (!bCryptPasswordEncoder.matches(loginRequest.getPassword(), user.getPassword()))
            throw new InvalidLoginDetailsException();

        return loginUser(userService.findByUsername(loginRequest.getUsername()));
    }

    public LoginResponse registerWithUsernameAndPassword(RegisterRequest registerRequest) {
        validateRegisterRequest(registerRequest);
        UserDTO user = userService.createNewUser(registerRequest);

        return loginUser(user);
    }

    private LoginResponse loginUser(UserDTO user) {
        return new LoginResponse(jwtTokenGenerator.generateTokenWithPrefix(user));
    }

    private void validateRegisterRequest(RegisterRequest registerRequest) {
        if (registerRequest.getUsername() == null || registerRequest.getPassword() == null || registerRequest.getEmail() == null ||
                registerRequest.getFirstName() == null || registerRequest.getLastName() == null || !SecurityHelper.isPasswordvalid(registerRequest.getPassword()))
            throw new InvalidRegisterDetailsException("Invalid or missing mandatory fields.");
        if (userRepository.findByUsernameOrEmail(registerRequest.getUsername(), registerRequest.getEmail()).isPresent())
            throw new AlreadyExistingAccountException("There's already an account with that username or email.");
    }
}

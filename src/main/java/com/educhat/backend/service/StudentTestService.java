package com.educhat.backend.service;

import com.educhat.backend.datamodel.dto.StudentTestDTO;
import com.educhat.backend.datamodel.dto.TestCompletionDTO;
import com.educhat.backend.datamodel.dto.UserDTO;
import com.educhat.backend.datamodel.entity.GroupMember;
import com.educhat.backend.datamodel.entity.StudentTest;
import com.educhat.backend.datamodel.entity.TestCompletion;
import com.educhat.backend.datamodel.entity.User;
import com.educhat.backend.datamodel.request.CompleteTestRequest;
import com.educhat.backend.datamodel.request.ScoreTestCompletionRequest;
import com.educhat.backend.datamodel.request.StudentTestCreateRequest;
import com.educhat.backend.exception.ResourceNoLongerExistingException;
import com.educhat.backend.exception.TestAlreadyCompletedException;
import com.educhat.backend.repository.*;
import com.educhat.backend.util.EntityCreationHelper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentTestService {
    StudentTestRepository studentTestRepository;
    TestCompletionRepository testCompletionRepository;
    UserService userService;
    GroupMemberService groupMemberService;
    GroupService groupService;
    ModelMapper modelMapper;

    public StudentTestService(StudentTestRepository studentTestRepository,
                              TestCompletionRepository testCompletionRepository, UserService userService,
                              GroupMemberService groupMemberService, GroupService groupService,
                              ModelMapper modelMapper) {
        this.studentTestRepository = studentTestRepository;
        this.testCompletionRepository = testCompletionRepository;
        this.userService = userService;
        this.groupMemberService = groupMemberService;
        this.groupService = groupService;
        this.modelMapper = modelMapper;
    }

    public StudentTestDTO createStudentTest(UserDTO userDTO, StudentTestCreateRequest request, String groupId) {
        StudentTest studentTest = new StudentTest(request.getTestName(), request.getParseableString(),
                Timestamp.from(Instant.now()), request.getDeadline(), request.getHardDeadline(),
                EntityCreationHelper.createUniqueBusinessId());
        GroupMember member = groupMemberService.getMembershipOfInGroup(userDTO, groupId);

        studentTest.setCreatedBy(member);
        StudentTestDTO result = modelMapper.map(studentTestRepository.save(studentTest), StudentTestDTO.class);
        result.setCreatedBy(userDTO);
        return result;
    }

    public boolean isTestAvailableToComplete(String testBusinessId) {
        return isTestAvailableToComplete(getStudentTestById(testBusinessId));
    }

    public boolean endTestPeriod(String testBusinessId) {
        StudentTest studentTest = getStudentTestById(testBusinessId);
        if (isTestAvailableToComplete(studentTest)) {
            studentTest.setAvailable(false);
            studentTestRepository.save(studentTest);
            return true;
        }
        return false;
    }

    public Collection<StudentTestDTO> getAvailableTestFromGroup(String groupBusinessId) {
        return groupService.getGroupByBusinessIdInternal(groupBusinessId).getGroupMembers().stream()
                .flatMap(groupMember -> groupMember.getCreatedTests().stream())
                .filter(this::isTestAvailableToComplete)
                .map(studentTest -> {
                    StudentTestDTO studentTestDTO = modelMapper.map(studentTest, StudentTestDTO.class);
                    studentTestDTO.setCreatedBy(modelMapper.map(studentTest.getCreatedBy().getAssociatedUser(),
                            UserDTO.class));
                    return studentTestDTO;
                })
                .collect(Collectors.toList());
    }

    public void completeStudentTest(UserDTO userDTO, String testBusinessId, CompleteTestRequest request,
                                    String groupBusinessId) {
        StudentTest studentTest = getStudentTestById(testBusinessId);
        GroupMember groupMember = groupMemberService.getMembershipOfInGroup(userDTO, groupBusinessId);
        if (testCompletionRepository.findByAssociatedTest_BusinessIdAndCompletedBy_Id(testBusinessId,
                groupMember.getId()).isPresent())
            throw new TestAlreadyCompletedException();

        TestCompletion testCompletion = new TestCompletion(Timestamp.from(Instant.now()), request.getSolutionString()
                , EntityCreationHelper.createUniqueBusinessId(), studentTest, groupMember);
        testCompletionRepository.save(testCompletion);
    }

    public void scoreStudentTest(ScoreTestCompletionRequest request, String testId, String groupdId) {
        TestCompletion testCompletion =
                testCompletionRepository.findByAssociatedTest_BusinessIdAndAndCompletedBy_Group_BusinessId(testId,
                        groupdId).orElseThrow(ResourceNoLongerExistingException::new);

        if(request.getResult()!=null)
            testCompletion.setResult(request.getResult());
        if(request.getComment()!=null)
            testCompletion.setTeacherComment(request.getComment());
        testCompletionRepository.save(testCompletion);
    }

    public List<TestCompletionDTO> findAllCompletionsOfTestInGroup(String testId, String groupId){
        return testCompletionRepository.findAllByAssociatedTest_BusinessIdAndAssociatedTest_CreatedBy_Group_BusinessId(testId,groupId).stream()
                .map(testCompletion -> modelMapper.map(testCompletion, TestCompletionDTO.class)).collect(Collectors.toList());
    }

    protected StudentTest getStudentTestById(String testBusinessId) {
        return studentTestRepository.findByBusinessId(testBusinessId).orElseThrow(ResourceNoLongerExistingException::new);
    }

    private boolean isTestAvailableToComplete(StudentTest studentTest) {
        return studentTest.getAvailable() && (!studentTest.getDeadline().before(Timestamp.from(Instant.now())) || !studentTest.getHardDeadline());
    }
}

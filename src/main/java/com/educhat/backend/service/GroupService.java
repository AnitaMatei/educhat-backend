package com.educhat.backend.service;

import com.educhat.backend.datamodel.dto.GroupDTO;
import com.educhat.backend.datamodel.dto.UserDTO;
import com.educhat.backend.datamodel.entity.Group;
import com.educhat.backend.datamodel.entity.GroupMember;
import com.educhat.backend.datamodel.entity.GroupMemberBusinessRole;
import com.educhat.backend.datamodel.entity.User;
import com.educhat.backend.datamodel.request.GroupCreateRequest;
import com.educhat.backend.exception.ResourceNoLongerExistingException;
import com.educhat.backend.repository.*;
import com.educhat.backend.util.EntityCreationHelper;
import com.educhat.backend.util.IBusinessRoleConstants;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;

@Service
public class GroupService {
    GroupRepository groupRepository;
    UserService userService;
    BusinessRoleRepository businessRoleRepository;
    GroupMemberService groupMemberService;
    ModelMapper modelMapper;


    public GroupService(GroupRepository groupRepository, UserService userService,
                        BusinessRoleRepository businessRoleRepository, GroupMemberService groupMemberService,
                        ModelMapper modelMapper) {
        this.groupRepository = groupRepository;
        this.userService = userService;
        this.businessRoleRepository = businessRoleRepository;
        this.groupMemberService = groupMemberService;
        this.modelMapper = modelMapper;
    }

    public GroupDTO createGroup(UserDTO userDTO, GroupCreateRequest request) {
        Timestamp now = Timestamp.from(Instant.now());
        String businessId = EntityCreationHelper.createUniqueBusinessId();
        User user = userService.findUserByEmailOrUsernameInternal(userDTO);

        GroupMember groupMember = new GroupMember(now, user,
                new GroupMemberBusinessRole(IBusinessRoleConstants.TEACHER,
                        businessRoleRepository.findByRoleName(IBusinessRoleConstants.TEACHER).get()));
        Group group = new Group(request.getGroupName(), now, businessId, request.getPictureUrl(), user);
        groupMember.getGroupRole().setMember(groupMember);
        groupMember.setGroup(group);
        group.getGroupMembers().add(groupMember);

        GroupDTO createdGroup = modelMapper.map(groupRepository.save(group), GroupDTO.class);
        createdGroup.setCreatedBy(userDTO);
        return createdGroup;
    }

    public GroupDTO getGroupByBusinessId(String groupId) {
        Group group = getGroupByBusinessIdInternal(groupId);
        GroupDTO result = modelMapper.map(group, GroupDTO.class);
        result.setCreatedBy(modelMapper.map(group.getCreatedBy(), UserDTO.class));
        result.setMembers(groupMemberService.getGroupMembersOf(group));

        return result;
    }

    protected Group getGroupByBusinessIdInternal(String groupBusinessId) {
        return groupRepository.findByBusinessId(groupBusinessId).orElseThrow(ResourceNoLongerExistingException::new);
    }
}

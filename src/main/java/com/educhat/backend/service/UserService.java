package com.educhat.backend.service;

import com.educhat.backend.datamodel.dto.SecurityRoleDTO;
import com.educhat.backend.datamodel.dto.UserDTO;
import com.educhat.backend.datamodel.entity.BusinessRole;
import com.educhat.backend.datamodel.entity.User;
import com.educhat.backend.datamodel.request.RegisterRequest;
import com.educhat.backend.exception.InvalidRoleException;
import com.educhat.backend.exception.ResourceNoLongerExistingException;
import com.educhat.backend.repository.BusinessRoleRepository;
import com.educhat.backend.repository.SecurityRoleRepository;
import com.educhat.backend.repository.UserRepository;
import com.educhat.backend.security.IRoleConstants;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserService {
    UserRepository userRepository;
    SecurityRoleRepository securityRoleRepository;
    BusinessRoleRepository businessRoleRepository;
    BCryptPasswordEncoder bCryptPasswordEncoder;
    ModelMapper modelMapper;

    public UserService(UserRepository userRepository, SecurityRoleRepository securityRoleRepository,
                       BusinessRoleRepository businessRoleRepository, BCryptPasswordEncoder bCryptPasswordEncoder,
                       ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.securityRoleRepository = securityRoleRepository;
        this.businessRoleRepository = businessRoleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.modelMapper = modelMapper;
    }

    public UserDTO findByUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        UserDTO result = user.map(value -> modelMapper.map(value, UserDTO.class)).orElse(null);
        if (result != null) {
            result.setSecurityRoles(securityRoleRepository.findAllByUsersWithRole(user.get()).stream().map(role -> modelMapper.map(role, SecurityRoleDTO.class)).collect(Collectors.toSet()));
        }
        return result;

    }

    public UserDTO createNewUser(RegisterRequest registerRequest) {
        User user = new User(registerRequest,
                Set.of(securityRoleRepository.findByRoleName(IRoleConstants.USER_ROLE_STRING)));
        user.setPassword(bCryptPasswordEncoder.encode(registerRequest.getPassword()));
        return modelMapper.map(userRepository.save(user), UserDTO.class);
    }

    public boolean hasBusinessRole(UserDTO userDTO, String role) {
        BusinessRole businessRole = businessRoleRepository.findByRoleName(role).orElseThrow(InvalidRoleException::new);
        User user = findUserByEmailOrUsernameInternal(userDTO);

        return user.getGroupMemberships().stream().anyMatch(groupMember -> groupMember.getGroupRole().getRole().equals(businessRole));
    }

    protected User findUserByEmailOrUsernameInternal(UserDTO userDTO) {
        return userRepository.findByUsernameOrEmail(userDTO.getUsername(), userDTO.getEmail()).orElseThrow(ResourceNoLongerExistingException::new);
    }
}

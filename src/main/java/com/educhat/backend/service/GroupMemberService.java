package com.educhat.backend.service;

import com.educhat.backend.datamodel.dto.BusinessRoleDTO;
import com.educhat.backend.datamodel.dto.GroupMemberBusinessRoleDTO;
import com.educhat.backend.datamodel.dto.GroupMemberDTO;
import com.educhat.backend.datamodel.dto.UserDTO;
import com.educhat.backend.datamodel.entity.Group;
import com.educhat.backend.datamodel.entity.GroupMember;
import com.educhat.backend.datamodel.entity.User;
import com.educhat.backend.exception.ResourceNoLongerExistingException;
import com.educhat.backend.repository.GroupMemberRepository;
import com.educhat.backend.repository.GroupRepository;
import com.educhat.backend.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupMemberService {
    GroupMemberRepository groupMemberRepository;
    UserRepository userRepository;
    GroupRepository groupRepository;
    ModelMapper modelMapper;

    public GroupMemberService(GroupMemberRepository groupMemberRepository, UserRepository userRepository,
                              GroupRepository groupRepository, ModelMapper modelMapper) {
        this.groupMemberRepository = groupMemberRepository;
        this.userRepository = userRepository;
        this.groupRepository = groupRepository;
        this.modelMapper = modelMapper;
    }

    public boolean doesMemberHaveBusinessPrivilege(UserDTO userDTO, String groupId, String privilege) {
        User user =
                userRepository.findByUsername(userDTO.getUsername()).orElseThrow(ResourceNoLongerExistingException::new);
        return user.getGroupMemberships().stream().anyMatch(
                groupMember -> groupMember.getGroup().getBusinessId().equals(groupId) && groupMember.getGroupRole().getRole().getPrivileges().stream().anyMatch(
                        businessPrivilege -> businessPrivilege.getPrivilegeName().equals(privilege)));

    }

    public boolean isUserMemberOfGroup(UserDTO userDTO, String groupId){
        User user =
                userRepository.findByUsername(userDTO.getUsername()).orElseThrow(ResourceNoLongerExistingException::new);

        return user.getGroupMemberships().stream().anyMatch(groupMember -> groupMember.getGroup().getBusinessId().equals(groupId));
    }

    public Collection<GroupMemberDTO> getGroupMembersOf(Group group){
        Collection<GroupMember> groupMembers = group.getGroupMembers();
        return groupMembers.stream().map(groupMember -> {
            //TODO: write some converters to ease this pain....
            GroupMemberDTO groupMemberDTO = modelMapper.map(groupMember, GroupMemberDTO.class);
            BusinessRoleDTO businessRoleDTO = modelMapper.map((groupMember.getGroupRole().getRole()), BusinessRoleDTO.class);
            GroupMemberBusinessRoleDTO groupMemberBusinessRoleDTO = modelMapper.map(groupMember.getGroupRole(), GroupMemberBusinessRoleDTO.class);
            groupMemberBusinessRoleDTO.setRole(businessRoleDTO);
            groupMemberDTO.setGroupRole(groupMemberBusinessRoleDTO);
            groupMemberDTO.setAssociatedUser(groupMember.getAssociatedUser().getUsername());
            return groupMemberDTO;
        }).collect(Collectors.toList());
    }

    protected GroupMember getMembershipOfInGroup(UserDTO userDTO, String groupId) {
        return getMembershipOfInGroup(userDTO.getUsername(), groupId);
    }

    protected GroupMember getMembershipOfInGroup(String username, String groupId) {
        return groupMemberRepository.findByAssociatedUser_UsernameAndGroup_BusinessId(username, groupId).orElseThrow(ResourceNoLongerExistingException::new);
    }

}

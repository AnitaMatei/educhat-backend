package com.educhat.backend.service;

import com.educhat.backend.datamodel.dto.GroupDTO;
import com.educhat.backend.datamodel.dto.GroupInviteDTO;
import com.educhat.backend.datamodel.dto.UserDTO;
import com.educhat.backend.datamodel.entity.*;
import com.educhat.backend.datamodel.request.GroupInviteRequest;
import com.educhat.backend.exception.InvalidInviteException;
import com.educhat.backend.exception.InvalidRoleException;
import com.educhat.backend.exception.ResourceNoLongerExistingException;
import com.educhat.backend.repository.*;
import com.educhat.backend.util.EntityCreationHelper;
import com.educhat.backend.util.IBusinessRoleConstants;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.Instant;

@Service
public class GroupInviteService {
    GroupRepository groupRepository;
    GroupMemberRepository groupMemberRepository;
    UserRepository userRepository;
    BusinessRoleRepository businessRoleRepository;
    GroupInviteRepository groupInviteRepository;
    GroupMemberService groupMemberService;
    ModelMapper modelMapper;

    public GroupInviteService(GroupRepository groupRepository, GroupMemberRepository groupMemberRepository,
                              UserRepository userRepository, BusinessRoleRepository businessRoleRepository,
                              GroupInviteRepository groupInviteRepository, GroupMemberService groupMemberService,
                              ModelMapper modelMapper) {
        this.groupRepository = groupRepository;
        this.groupMemberRepository = groupMemberRepository;
        this.userRepository = userRepository;
        this.businessRoleRepository = businessRoleRepository;
        this.groupInviteRepository = groupInviteRepository;
        this.groupMemberService = groupMemberService;
        this.modelMapper = modelMapper;
    }

    public GroupInviteDTO createGroupInvite(UserDTO userDTO, GroupInviteRequest request, String groupId) {
        String businessId = EntityCreationHelper.createUniqueBusinessId();
        User user = getUser(userDTO);
        Group group = groupRepository.findByBusinessId(groupId).orElseThrow(ResourceNoLongerExistingException::new);
        BusinessRole role =
                businessRoleRepository.findByRoleName(request.getForRole()).orElseThrow(InvalidRoleException::new);

        GroupInviteDTO result = modelMapper.map(groupInviteRepository.save(new GroupInvite(businessId, user, group,
                role, Timestamp.from(Instant.now()))), GroupInviteDTO.class);

        result.setCreatedByUser(modelMapper.map(user, UserDTO.class));
        result.setToGroup(modelMapper.map(group, GroupDTO.class));

        return result;
    }

    public void acceptGroupInvite(UserDTO user, String groupInviteId) {
        GroupInvite groupInvite = groupInviteRepository.findByBusinessId(groupInviteId).orElseThrow(InvalidInviteException::new);
        if(groupMemberService.isUserMemberOfGroup(user, groupInvite.getToGroup().getBusinessId()))
            throw new InvalidInviteException();
        if(groupInvite.isUsed())
            throw new InvalidInviteException();
        GroupMember groupMember = new GroupMember(Timestamp.from(Instant.now()), getUser(user), new GroupMemberBusinessRole(groupInvite.getForRole().getRoleName(), groupInvite.getForRole()));
        groupMember.getGroupRole().setMember(groupMember);
        groupMember.setGroup(groupInvite.getToGroup());

        groupInvite.setUsed(true);

        groupMemberRepository.save(groupMember);
    }

    private User getUser(UserDTO user) {
        return userRepository.findByUsernameOrEmail(user.getUsername(), user.getEmail()).orElseThrow(ResourceNoLongerExistingException::new);
    }
}

package com.educhat.backend.service;

import com.educhat.backend.datamodel.dto.MessageDTO;
import com.educhat.backend.datamodel.dto.UserDTO;
import com.educhat.backend.datamodel.entity.GroupMember;
import com.educhat.backend.datamodel.entity.Message;
import com.educhat.backend.datamodel.request.MessageCreationRequest;
import com.educhat.backend.exception.ErrorSendingMessage;
import com.educhat.backend.repository.MessageRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;

@Service
@Slf4j
public class MessageService {
    MessageRepository messageRepository;
    GroupMemberService groupMemberService;
    ModelMapper modelMapper;
    SimpMessagingTemplate simpMessagingTemplate;

    public MessageService(MessageRepository messageRepository, GroupMemberService groupMemberService,
                          ModelMapper modelMapper, SimpMessagingTemplate simpMessagingTemplate) {
        this.messageRepository = messageRepository;
        this.groupMemberService = groupMemberService;
        this.modelMapper = modelMapper;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    public MessageDTO sendMessageAll(UserDTO userDTO, MessageCreationRequest request) {
        GroupMember groupMember = groupMemberService.getMembershipOfInGroup(userDTO, request.getGroupId());
        Message message = new Message(Timestamp.from(Instant.now()), false, request.getMessageText(), groupMember,
                null);
        MessageDTO result;
        try {
            result = modelMapper.map(messageRepository.save(message),MessageDTO.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            //TODO: more details based on except...
            throw new ErrorSendingMessage("Error sending message.");
        }
        result.setSentBy(userDTO.getUsername());
        return result;
    }

    public void sendMessagePrivate(UserDTO userDTO, MessageCreationRequest request) {
        GroupMember groupMemberSender = groupMemberService.getMembershipOfInGroup(userDTO, request.getGroupId());
        GroupMember groupMemberRecipient = groupMemberService.getMembershipOfInGroup(request.getSentTo(), request.getGroupId());
        Message message = new Message(Timestamp.from(Instant.now()), true, request.getMessageText(),
                groupMemberSender, groupMemberRecipient);
        MessageDTO result;
        try {
            result = modelMapper.map(messageRepository.save(message),MessageDTO.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            //TODO: more details based on except...
            throw new ErrorSendingMessage("Error sending message.");
        }
        result.setSentBy(userDTO.getUsername());
        simpMessagingTemplate.convertAndSendToUser(userDTO.getUsername(), "/secured/user/queue/specific-user", result);
    }
}

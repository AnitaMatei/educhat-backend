package com.educhat.backend.util;

import java.util.UUID;

public final class EntityCreationHelper {
    public static String createUniqueBusinessId(){
        return UUID.randomUUID().toString().replace("-","");
    }
}

package com.educhat.backend.util;

public interface IBusinessRoleConstants {
    String TEACHER = "TEACHER";
    String ASSISTANT = "ASSISTANT";
    String STUDENT = "STUDENT";
    String GUEST = "GUEST";

    interface Privileges{
        String CREATE_TEST = "CREATE_TEST";
        String EDIT_TEST = "EDIT_TEST";
        String RETRIEVE_TEST_ALL = "RETRIEVE_TEST_ALL";
        String RETRIEVE_TEST_ACTIVE = "RETRIEVE_TEST_ACTIVE";
        String COMPLETE_TEST = "COMPLETE_TEST";
        String READ_TEST_SCORE_ALL = "READ_TEST_SCORE_ALL";
        String READ_TEST_SCORE_OWN = "READ_TEST_SCORE_OWN";
        String READ_TEST_COMMENT_ALL = "READ_TEST_COMMENT_ALL";
        String READ_TEST_COMMENT_OWN = "READ_TEST_COMMENT_OWN";
        String ADD_TEST_COMMENT = "ADD_TEST_COMMENT";
        String ADD_TEST_SCORE = "ADD_TEST_SCORE";
        String SEND_MESSAGE_ALL = "SEND_MESSAGE_ALL";
        String SEND_MESSAGE_PRIVATE = "SEND_MESSAGE_PRIVATE";
        String READ_MESSAGE_PRIVATE_ALL = "READ_MESSAGE_PRIVATE_ALL";
        String INVITE_MEMBERS = "INVITE_MEMBERS";
    }
}

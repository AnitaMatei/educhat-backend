package com.educhat.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EduchatApplication {

    public static void main(String[] args) {
        SpringApplication.run(EduchatApplication.class, args);
    }

}

package com.educhat.backend.datamodel.dto;

import lombok.Data;

@Data
public class BusinessRoleDTO {
    private String roleName;
}

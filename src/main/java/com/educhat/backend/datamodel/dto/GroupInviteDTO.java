package com.educhat.backend.datamodel.dto;

import com.educhat.backend.datamodel.entity.BusinessRole;
import com.educhat.backend.datamodel.entity.Group;
import com.educhat.backend.datamodel.entity.User;
import lombok.Data;

import javax.persistence.*;

@Data
public class GroupInviteDTO {
    private String businessId;
    private boolean used = false;
    private java.sql.Timestamp createdAt;
    private UserDTO createdByUser;
    private GroupDTO toGroup;
}

package com.educhat.backend.datamodel.dto;

import lombok.Data;

@Data
public class TestCompletionDTO {
    private java.sql.Timestamp completedAt;
    private Long result;
    private String teacherComment;
    private String solutionString;
    private String businessId;
}

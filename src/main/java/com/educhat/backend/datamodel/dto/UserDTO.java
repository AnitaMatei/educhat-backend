package com.educhat.backend.datamodel.dto;

import com.educhat.backend.datamodel.entity.GroupMember;
import com.educhat.backend.datamodel.entity.User;
import lombok.Data;

import java.util.Collection;
import java.util.HashSet;

@Data
public class UserDTO {
    private String username;
    private String email;
    private String firstName;
    private String lastName;
    private String middleName;
    private boolean banned;
    private Collection<SecurityRoleDTO> securityRoles;
    private Collection<GroupMemberDTO> groupMemberships;
}

package com.educhat.backend.datamodel.dto;

import lombok.Data;

import java.sql.Time;
import java.sql.Timestamp;

@Data
public class StudentTestDTO {
    private String testName;
    private String parseableString;
    private Timestamp createdAt;
    private UserDTO createdBy;
    private Timestamp deadline;
    private Boolean hardDeadline;
    private String businessId;
    private Boolean available;
}

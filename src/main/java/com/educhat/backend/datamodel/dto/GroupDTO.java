package com.educhat.backend.datamodel.dto;

import com.educhat.backend.datamodel.entity.Group;
import com.educhat.backend.datamodel.entity.GroupMember;
import com.educhat.backend.datamodel.entity.User;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Collection;

@Data
public class GroupDTO {
    private String groupName;
    private java.sql.Timestamp createdAt;
    private String businessId;
    private String pictureUrl;
    private UserDTO createdBy;
    private Collection<GroupMemberDTO> members;
}

package com.educhat.backend.datamodel.dto;

import lombok.Data;

@Data
public class SecurityRoleDTO {
    private String roleName;
}

package com.educhat.backend.datamodel.dto;

import com.educhat.backend.datamodel.entity.GroupMember;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
public class MessageDTO {
    private java.sql.Timestamp sentAt;
    private String messageText;
    private String sentBy;
}

package com.educhat.backend.datamodel.dto;

import com.educhat.backend.datamodel.entity.BusinessRole;
import lombok.Data;

@Data
public class GroupMemberBusinessRoleDTO {
    private String roleNickname;
    private BusinessRoleDTO role;
}

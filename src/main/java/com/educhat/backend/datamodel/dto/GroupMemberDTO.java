package com.educhat.backend.datamodel.dto;

import com.educhat.backend.datamodel.entity.*;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Collection;

@Data
public class GroupMemberDTO {
    private java.sql.Timestamp joinDate;
    private String associatedUser;
    private GroupMemberBusinessRoleDTO groupRole;
}

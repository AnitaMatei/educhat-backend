package com.educhat.backend.datamodel.request;

import lombok.Data;

@Data
public class ScoreTestCompletionRequest {
    private Long result;
    private String comment;
}

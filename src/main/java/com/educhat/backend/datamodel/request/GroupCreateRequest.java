package com.educhat.backend.datamodel.request;

import lombok.Data;

@Data
public class GroupCreateRequest {
    private String groupName;
    private String pictureUrl;
}

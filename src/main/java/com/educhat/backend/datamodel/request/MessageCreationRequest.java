package com.educhat.backend.datamodel.request;

import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotEmpty;

@Data
public class MessageCreationRequest {
    @NotEmpty
    private String messageText;
    @NotEmpty
    private String sentBy;
    private String sentTo;
    private Boolean isPrivate = false;
    @NotEmpty
    private String groupId;
}

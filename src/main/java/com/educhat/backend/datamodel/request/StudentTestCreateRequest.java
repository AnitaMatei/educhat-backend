package com.educhat.backend.datamodel.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class StudentTestCreateRequest {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    Timestamp deadline;
    String parseableString;
    String testName;
    Boolean hardDeadline;
}

package com.educhat.backend.datamodel.request;

import lombok.Data;

@Data
public class CompleteTestRequest {
    String solutionString;
}

package com.educhat.backend.datamodel.request;

import lombok.Data;

@Data
public class LoginRequest {
    String username;
    String password;
}

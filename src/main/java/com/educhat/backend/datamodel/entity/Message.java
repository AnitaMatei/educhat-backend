package com.educhat.backend.datamodel.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "message")
public class Message {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "message_id")
  private Long messageId;
  private java.sql.Timestamp sentAt;
  private Boolean isPrivate;
  private String messageText;

  @ManyToOne
  @JoinColumn(name = "sent_by")
  @JsonManagedReference
  private GroupMember sentBy;
  @ManyToOne
  @JoinColumn(name = "sent_to")
  @JsonManagedReference
  private GroupMember sentTo;

  public Message(Timestamp sentAt, Boolean isPrivate, String messageText, GroupMember sentBy, GroupMember sentTo) {
    this.sentAt = sentAt;
    this.isPrivate = isPrivate;
    this.messageText = messageText;
    this.sentBy = sentBy;
    this.sentTo = sentTo;
  }
}

package com.educhat.backend.datamodel.entity;


import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "business_privilege")
public class BusinessPrivilege {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "business_privilege_id")
  private Long id;
  private String privilegeName;

  @ManyToMany
  @JoinTable(
          name = "business_role_privilege",
          joinColumns = @JoinColumn(
                  name = "business_privilege_id"),
          inverseJoinColumns = @JoinColumn(
                  name = "business_role_id"))
  @ToString.Exclude
  private Collection<BusinessRole> rolesWithPrivilege;
}

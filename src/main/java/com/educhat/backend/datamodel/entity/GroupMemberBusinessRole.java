package com.educhat.backend.datamodel.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "group_member_business_role")
public class GroupMemberBusinessRole {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "group_member_business_role_id")
  private Long id;
  private String roleNickname;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "business_role_id")
  private BusinessRole role;
  @OneToOne
  @JoinColumn(name = "group_member_id")
  private GroupMember member;

  public GroupMemberBusinessRole(String roleNickname, BusinessRole role) {
    this.roleNickname = roleNickname;
    this.role = role;
  }
}

package com.educhat.backend.datamodel.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "group_member")
public class GroupMember {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "group_member_id")
  private Long id;
  private java.sql.Timestamp joinDate;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user_id")
  @JsonManagedReference
  private User associatedUser;
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "group_id")
  private Group group;
  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "member")
  private GroupMemberBusinessRole groupRole;
  @OneToMany(mappedBy = "createdBy")
  @ToString.Exclude
  @JsonBackReference
  private Collection<StudentTest> createdTests = new HashSet<>();
  @OneToMany(mappedBy = "completedBy")
  @ToString.Exclude
  @JsonBackReference
  private Collection<TestCompletion> testsCompleted = new HashSet<>();
  @OneToMany(mappedBy = "sentBy")
  @ToString.Exclude
  @JsonBackReference
  private Collection<Message> sentMessages;
  @OneToMany(mappedBy = "sentTo")
  @ToString.Exclude
  @JsonBackReference
  private Collection<Message> receivedMessages;

  public GroupMember(Timestamp joinDate, User associatedUser, GroupMemberBusinessRole groupRole) {
    this.joinDate = joinDate;
    this.associatedUser = associatedUser;
    this.groupRole = groupRole;
  }
}

package com.educhat.backend.datamodel.entity;

import com.educhat.backend.datamodel.entity.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "group_invite")
public class GroupInvite {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "group_invite_id")
  private Long id;
  private String businessId;
  private boolean used = false;
  private java.sql.Timestamp createdAt;


  @ManyToOne
  @JoinColumn(name = "created_by_user_id")
  private User createdByUser;
  @ManyToOne
  @JoinColumn(name = "to_group_id")
  private Group toGroup;
  @ManyToOne
  @JoinColumn(name = "for_role_id")
  private BusinessRole forRole;

  public GroupInvite(String businessId, User createdByUser, Group toGroup, BusinessRole forRole, Timestamp createdAt) {
    this.businessId = businessId;
    this.createdByUser = createdByUser;
    this.toGroup = toGroup;
    this.forRole = forRole;
    this.createdAt = createdAt;
  }
}

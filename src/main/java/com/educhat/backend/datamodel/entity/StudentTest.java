package com.educhat.backend.datamodel.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;

@Getter
@Setter
@ToString
@Entity
@Table(name = "student_test")
@NoArgsConstructor
public class StudentTest {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "student_test_id")
  private Long id;
  private String testName;
  private String parseableString;
  private java.sql.Timestamp createdAt;
  private java.sql.Timestamp deadline;
  private Boolean hardDeadline;
  private String businessId;
  private Boolean available = true;

  @ManyToOne
  @JoinColumn(name = "created_by")
  private GroupMember createdBy;
  @OneToMany(mappedBy = "associatedTest")
  @ToString.Exclude
  @JsonManagedReference
  private Collection<TestCompletion> testCompletions = new HashSet<>();

  public StudentTest(String testName, String parseableString, Timestamp createdAt, Timestamp deadline,
                     Boolean hardDeadline, String businessId) {
    this.testName = testName;
    this.parseableString = parseableString;
    this.createdAt = createdAt;
    this.deadline = deadline;
    this.hardDeadline = hardDeadline;
    this.businessId = businessId;
  }
}

package com.educhat.backend.datamodel.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;

@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "`group`")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_id")
    private Long groupId;
    private String groupName;
    private java.sql.Timestamp createdAt;
    @Column(name = "business_id")
    private String businessId;
    private String pictureUrl;

    @ManyToOne
    @JoinColumn(name = "created_by_user_id")
    private User createdBy;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group")
    @ToString.Exclude
    private Collection<GroupMember> groupMembers = new HashSet<>();
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "toGroup")
    @ToString.Exclude
    private Collection<GroupInvite> invitesToGroup = new HashSet<>();

    public Group(String groupName, Timestamp createdAt, String businessId, String pictureUrl, User createdBy) {
        this.groupName = groupName;
        this.createdAt = createdAt;
        this.businessId = businessId;
        this.pictureUrl = pictureUrl;
        this.createdBy = createdBy;
    }
}

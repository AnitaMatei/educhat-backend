package com.educhat.backend.datamodel.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "test_completion")
public class TestCompletion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "test_completion_id")
    private Long id;
    private java.sql.Timestamp completedAt;
    private Long result = 0L;
    private String teacherComment = "";
    private String solutionString;
    private String businessId;

    @ManyToOne
    @JoinColumn(name = "test_completed_id")
    private StudentTest associatedTest;
    @ManyToOne
    @JoinColumn(name = "completed_by")
    @JsonManagedReference
    private GroupMember completedBy;

    public TestCompletion(Timestamp completedAt, String solutionString, String businessId, StudentTest associatedTest,
                          GroupMember completedBy) {
        this.completedAt = completedAt;
        this.solutionString = solutionString;
        this.businessId = businessId;
        this.associatedTest = associatedTest;
        this.completedBy = completedBy;
    }
}

package com.educhat.backend.datamodel.entity;

import com.educhat.backend.datamodel.request.RegisterRequest;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    private String username;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private String middleName;
    private boolean logoutNextTime = false;
    private boolean banned = false;

    public User(RegisterRequest registerRequest, Set<SecurityRole> roles) {
        this.username = registerRequest.getUsername();
        this.password = registerRequest.getPassword();
        this.email = registerRequest.getEmail();
        this.firstName = registerRequest.getFirstName();
        this.lastName = registerRequest.getLastName();
        this.middleName = registerRequest.getMiddleName();
        this.userRoles = roles;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_security_role",
            joinColumns = @JoinColumn(
                    name = "user_id"),
            inverseJoinColumns = @JoinColumn(
                    name = "security_role_id"))
    private Set<SecurityRole> userRoles = new HashSet<>();
    @OneToMany(mappedBy = "createdBy")
    @ToString.Exclude
    private Collection<Group> groupsCreated = new HashSet<>();
    @OneToMany(mappedBy = "associatedUser")
    @ToString.Exclude
    @JsonBackReference
    private Collection<GroupMember> groupMemberships = new HashSet<>();
    @OneToMany(mappedBy = "createdByUser")
    @ToString.Exclude
    private Collection<GroupInvite> groupInvites = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        User user = (User) o;
        return id != null && Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, firstName, lastName, middleName, userRoles);
    }
}

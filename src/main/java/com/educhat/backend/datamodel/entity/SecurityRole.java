package com.educhat.backend.datamodel.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "security_role")
public class SecurityRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "security_role_id")
    private Long id;
    private String roleName;

    @ManyToMany
    @JoinTable(
            name = "user_security_role",
            joinColumns = @JoinColumn(
                    name = "security_role_id"),
            inverseJoinColumns = @JoinColumn(
                    name = "user_id"))
    @ToString.Exclude
    private Collection<User> usersWithRole;
}

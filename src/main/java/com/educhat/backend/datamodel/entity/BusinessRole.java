package com.educhat.backend.datamodel.entity;


import lombok.*;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "business_role")
public class BusinessRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "business_role_id")
    private Long id;
    private String roleName;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "business_role_privilege",
            joinColumns = @JoinColumn(
                    name = "business_role_id"),
            inverseJoinColumns = @JoinColumn(
                    name = "business_privilege_id"))
    private Collection<BusinessPrivilege> privileges = new HashSet<>();
    @OneToMany(mappedBy = "role")
    @ToString.Exclude
    private Collection<GroupMemberBusinessRole> groupMembersWithRole = new HashSet<>();
    @OneToMany(mappedBy = "forRole")
    @ToString.Exclude
    private Collection<GroupInvite> groupInvitesForRole = new HashSet<>();

}

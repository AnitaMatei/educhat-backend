package com.educhat.backend.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidInviteException extends RuntimeException{
    public InvalidInviteException() {
        super("Invite may have already been used or point to a deleted group. (Or might already be a member!)");
    }
}

package com.educhat.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
public class TestAlreadyCompletedException extends RuntimeException {
    public TestAlreadyCompletedException() {
        super("You have already completed this test once.");
    }
}

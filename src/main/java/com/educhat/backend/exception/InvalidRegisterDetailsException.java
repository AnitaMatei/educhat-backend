package com.educhat.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidRegisterDetailsException extends RuntimeException{
    public InvalidRegisterDetailsException(String message) {
        super(message);
    }
}

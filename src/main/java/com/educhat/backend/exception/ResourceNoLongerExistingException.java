package com.educhat.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class ResourceNoLongerExistingException extends RuntimeException{
    public ResourceNoLongerExistingException() {
        super("A resource you tried to use/access does not exist anymore.");
    }
}

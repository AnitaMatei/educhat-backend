package com.educhat.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidRoleException extends RuntimeException{
    public InvalidRoleException() {
        super("The role you selected does not exist.");
    }
}

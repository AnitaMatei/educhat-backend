package com.educhat.backend.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN)
public class NoPrivilegeException extends RuntimeException{
    public NoPrivilegeException(String privilege) {
        super(String.format("You dont have the privilege: %s.", privilege));
    }
}

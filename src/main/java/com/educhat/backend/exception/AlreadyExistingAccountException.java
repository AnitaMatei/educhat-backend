package com.educhat.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
public class AlreadyExistingAccountException extends RuntimeException{
    public AlreadyExistingAccountException(String message) {
        super(message);
    }
}

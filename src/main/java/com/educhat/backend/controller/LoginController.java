package com.educhat.backend.controller;

import com.educhat.backend.datamodel.request.LoginRequest;
import com.educhat.backend.datamodel.request.LoginResponse;
import com.educhat.backend.service.AuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/login")
public class LoginController {
    AuthenticationService authenticationService;

    public LoginController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/username")
    public ResponseEntity<LoginResponse> loginWithUsernamePassword(@RequestBody LoginRequest loginRequest) {
        return ResponseEntity.ok(authenticationService.loginWithUsernameAndPassword(loginRequest));
    }
}

package com.educhat.backend.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class WelcomeController {

    @GetMapping("/hello/mynameis/{name}")
    public ResponseEntity<String> welcomeVisitor(@PathVariable(value = "name") String name){
        return ResponseEntity.ok("Hello  s a p"+name);
    }



}

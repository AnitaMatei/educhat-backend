package com.educhat.backend.controller;

import com.educhat.backend.controller.util.ControllerHelper;
import com.educhat.backend.datamodel.request.MessageCreationRequest;
import com.educhat.backend.service.MessageService;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class MessageController {
    MessageService messageService;

    @MessageMapping("/secured/sendMessageAll")
    @SendTo("/secured/topic/all")
    @PreAuthorize("isAuthenticated() and hasRole('USER') and canSendMessagesAll(#request.groupId)")
    public Object sendMessageAll(@Payload MessageCreationRequest request){
        return messageService.sendMessageAll(ControllerHelper.getLoggedInUser(), request);
    }


    @MessageMapping("/secured/sendMessagePrivate")
    @PreAuthorize("isAuthenticated() and hasRole('USER') and canSendMessagesPrivate(#request.groupId)")
    public void sendMessagePrivate(@Payload MessageCreationRequest request){
        messageService.sendMessagePrivate(ControllerHelper.getLoggedInUser(), request);
    }
}

package com.educhat.backend.controller;

import com.educhat.backend.controller.util.ControllerHelper;
import com.educhat.backend.datamodel.CustomUserDetails;
import com.educhat.backend.datamodel.dto.UserDTO;
import com.educhat.backend.datamodel.request.GroupCreateRequest;
import com.educhat.backend.datamodel.request.GroupInviteRequest;
import com.educhat.backend.service.GroupInviteService;
import com.educhat.backend.service.GroupService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/group")
public class GroupController {
    GroupService groupService;
    GroupInviteService groupInviteService;

    public GroupController(GroupService groupService, GroupInviteService groupInviteService) {
        this.groupService = groupService;
        this.groupInviteService = groupInviteService;
    }

    @PostMapping(path = "/create")
    @PreAuthorize("isAuthenticated() and hasRole('USER')")
    public ResponseEntity<Object> createGroup(@RequestBody GroupCreateRequest request) {
        return ResponseEntity.ok(groupService.createGroup(ControllerHelper.getLoggedInUser(), request));
    }

    @GetMapping(path= "/info/{groupId}")
    @PreAuthorize("isAuthenticated() and hasRole('USER') and isMemberOf(#groupId)")
    public ResponseEntity<Object> getGroupInfo(@PathVariable String groupId){
        return ResponseEntity.ok(groupService.getGroupByBusinessId(groupId));
    }

    @PostMapping(path = "/invite/{groupId}")
    @PreAuthorize("isAuthenticated() and hasRole('USER') and canInviteToGroup(#groupId)")
    public ResponseEntity<Object> inviteToGroup(@RequestBody GroupInviteRequest groupInviteRequest,
                                                @PathVariable String groupId) {
        return ResponseEntity.ok(groupInviteService.createGroupInvite(ControllerHelper.getLoggedInUser(), groupInviteRequest, groupId));
    }

    @PostMapping(path = "/join/{inviteId}")
    @PreAuthorize("isAuthenticated() and hasRole('USER')")
    public ResponseEntity<Void> acceptGroupInvitation(@PathVariable String inviteId) {
        groupInviteService.acceptGroupInvite(ControllerHelper.getLoggedInUser(), inviteId);
        return ResponseEntity.ok().build();
    }
}

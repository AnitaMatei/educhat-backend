package com.educhat.backend.controller;


import com.educhat.backend.controller.util.ControllerHelper;
import com.educhat.backend.datamodel.request.CompleteTestRequest;
import com.educhat.backend.datamodel.request.ScoreTestCompletionRequest;
import com.educhat.backend.datamodel.request.StudentTestCreateRequest;
import com.educhat.backend.service.StudentTestService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/test")
public class StudentTestController {
    StudentTestService studentTestService;

    public StudentTestController(StudentTestService studentTestService) {
        this.studentTestService = studentTestService;
    }

    @PostMapping("/create")
    @PreAuthorize("isAuthenticated() and hasRole('USER') and canCreateStudentTest(#groupId)")
    public ResponseEntity<Object> createStudentTest(@RequestParam String groupId,
                                                    @RequestBody StudentTestCreateRequest studentTestCreateRequest) {
        return ResponseEntity.ok(studentTestService.createStudentTest(ControllerHelper.getLoggedInUser(),
                studentTestCreateRequest, groupId));
    }

    @GetMapping("/allAvailable")
    @PreAuthorize("isAuthenticated() and hasRole('USER') and canRetrieveActiveStudentTests(#groupId)")
    public ResponseEntity<Object> getActiveStudentTests(@RequestParam String groupId) {
        return ResponseEntity.ok(studentTestService.getAvailableTestFromGroup(groupId));
    }

    @GetMapping("/isAvailable/{testId}")
    @PreAuthorize("isAuthenticated() and hasRole('USER') and canRetrieveActiveStudentTests(#groupId)")
    public ResponseEntity<Object> isTestActive(@PathVariable String testId, @RequestParam String groupId) {
        if (studentTestService.isTestAvailableToComplete(testId))
            return ResponseEntity.ok().build();
        else return ResponseEntity.ok().body("Test is not available anymore.");
    }

    @PostMapping("/complete/{testId}")
    @PreAuthorize("isAuthenticated() and hasRole('USER') and canCompleteTest(#groupId)")
    public ResponseEntity<Object> completeStudentTest(@PathVariable String testId, @RequestParam String groupId,
                                                      @RequestBody CompleteTestRequest request) {
        studentTestService.completeStudentTest(ControllerHelper.getLoggedInUser(), testId, request, groupId);
        return ResponseEntity.ok().body("Test successfully submitted.");
    }

    @PutMapping("/scoreTest/{testId}")
    @PreAuthorize("isAuthenticated() and hasRole('USER') and canScoreTest(#groupId, #request)")
    public ResponseEntity<Object> scoreStudentTest(@PathVariable String testId, @RequestParam String groupId,
                                                   @RequestBody @Valid ScoreTestCompletionRequest request) {
        studentTestService.scoreStudentTest(request, testId, groupId);
        return ResponseEntity.ok().body("Test successfully scored.");
    }

    @GetMapping("/completions/{testId}")
    @PreAuthorize("isAuthenticated() and hasRole('USER') and canRetrieveTestCompletions(#groupId)")
    public ResponseEntity<Object> retrieveAllTestCompletionsOf(@PathVariable String testId,
                                                               @RequestParam String groupId) {
        return ResponseEntity.ok(studentTestService.findAllCompletionsOfTestInGroup(testId, groupId));
    }

}

package com.educhat.backend.controller.util;

import com.educhat.backend.datamodel.CustomUserDetails;
import com.educhat.backend.datamodel.dto.UserDTO;
import org.springframework.security.core.context.SecurityContextHolder;

public class ControllerHelper {

    public static final UserDTO getLoggedInUser() {
        return ((CustomUserDetails) (SecurityContextHolder.getContext().getAuthentication().getPrincipal())).getUser();
    }
}

package com.educhat.backend.controller;

import com.educhat.backend.datamodel.request.LoginRequest;
import com.educhat.backend.datamodel.request.LoginResponse;
import com.educhat.backend.datamodel.request.RegisterRequest;
import com.educhat.backend.service.AuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/register")
public class RegistrationController {
    AuthenticationService authenticationService;

    public RegistrationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping
    public ResponseEntity<LoginResponse> registerWithUsernamePassword(@RequestBody RegisterRequest registerRequest){
        return ResponseEntity.ok(authenticationService.registerWithUsernameAndPassword(registerRequest));
    }

}

package com.educhat.backend.security.websocket;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class SocketBrokerConfig implements WebSocketMessageBrokerConfigurer {
    //TODO: investigate ease of deployment and use of kafka/rabbitmq for persistence broadcasting

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/secured/topic/all");
        config.enableSimpleBroker("/secured/user/queue/specific-user");
        config.setUserDestinationPrefix("/secured/user");
        config.setApplicationDestinationPrefixes("/appMessage");
    }
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/secured/sendMessageAll");
        registry.addEndpoint("/secured/sendMessagePrivate");
        //TODO: switch to android lib fallback
//        registry.addEndpoint("/secured/sendMessageAll").withSockJS();
    }
}

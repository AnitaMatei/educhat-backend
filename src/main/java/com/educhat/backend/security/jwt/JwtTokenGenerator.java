package com.educhat.backend.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.educhat.backend.datamodel.CustomUserDetails;
import com.educhat.backend.datamodel.dto.UserDTO;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.sql.Date;

@Component
@Slf4j
@Getter
public class JwtTokenGenerator {
    JwtTokenProperties jwtTokenProperties;

    public JwtTokenGenerator(JwtTokenProperties jwtTokenProperties) {
        this.jwtTokenProperties = jwtTokenProperties;
    }

    public String generateTokenWithPrefix(UserDTO user) {
        return jwtTokenProperties.getTokenPrefix() + " " + generateToken(user);
    }

    public String generateToken(UserDTO user) {
        return JWT.create()
                .withSubject(user.getUsername())
                .withIssuedAt(new Date(System.currentTimeMillis()))
                .withExpiresAt(new Date(System.currentTimeMillis() + jwtTokenProperties.getTokenExpirationTime()))
                .sign(Algorithm.HMAC512(jwtTokenProperties.getSecret().getBytes()));
    }

    public String getUsernameFromToken(String token) {
        return JWT.require(Algorithm.HMAC512(jwtTokenProperties.getSecret().getBytes()))
                .build()
                .verify(token.replace(jwtTokenProperties.getTokenPrefix()+" ", ""))
                .getSubject();
    }
}
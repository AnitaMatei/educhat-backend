package com.educhat.backend.security.jwt;

import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.educhat.backend.datamodel.CustomUserDetails;
import com.educhat.backend.datamodel.dto.UserDTO;
import com.educhat.backend.exception.InvalidLoginDetailsException;
import com.educhat.backend.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class JwtAuthorizationFilter extends BasicAuthenticationFilter {
    JwtTokenGenerator jwtTokenGenerator;
    JwtTokenProperties jwtTokenProperties;
    UserService userService;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager, JwtTokenGenerator jwtTokenGenerator,
                                  JwtTokenProperties jwtTokenProperties, UserService userService) {
        super(authenticationManager);
        this.jwtTokenGenerator = jwtTokenGenerator;
        this.jwtTokenProperties = jwtTokenProperties;
        this.userService = userService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String jwtToken = request.getHeader(jwtTokenProperties.getHeaderString());

        if (jwtToken != null && jwtToken.startsWith(jwtTokenProperties.getTokenPrefix())) {
            try {
                Authentication authentication = verifyJwtToken(request, jwtToken);
                SecurityContextHolder.getContext().setAuthentication(authentication);
            } catch (SignatureVerificationException | TokenExpiredException ex) {
                log.error(ex.getMessage());
            }
        }

        chain.doFilter(request, response);

    }

    private Authentication verifyJwtToken(HttpServletRequest request, String jwtToken) {
        String username = jwtTokenGenerator.getUsernameFromToken(jwtToken);
        UserDTO user = userService.findByUsername(username);

        if (user == null) {
            log.warn(String.format("{0} tried and failed to log in with user {1}.", request.getRemoteAddr(), username));
            throw new InvalidLoginDetailsException();
        }

        CustomUserDetails customUserDetails = new CustomUserDetails(user);
        return new UsernamePasswordAuthenticationToken(customUserDetails, null, customUserDetails.getAuthorities());
    }
}
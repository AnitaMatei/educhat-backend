package com.educhat.backend.security.jwt;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "security")
@PropertySource("classpath:security.yml")
@Getter
@Setter
public class JwtTokenProperties {
    @Value("${tokenExpirationTime}")
    private Long tokenExpirationTime;
    @Value("${secret}")
    private String secret;
    @Value("${tokenPrefix}")
    private String tokenPrefix;
    @Value("${headerString}")
    private String headerString;
}

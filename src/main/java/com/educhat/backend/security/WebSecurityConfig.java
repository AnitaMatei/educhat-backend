package com.educhat.backend.security;

import com.educhat.backend.security.jwt.JwtAuthorizationFilter;
import com.educhat.backend.security.jwt.JwtTokenGenerator;
import com.educhat.backend.security.jwt.JwtTokenProperties;
import com.educhat.backend.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    JwtTokenProperties jwtTokenProperties;
    JwtTokenGenerator jwtTokenGenerator;
    UserService userService;

    public WebSecurityConfig(JwtTokenProperties jwtTokenProperties,
                             JwtTokenGenerator jwtTokenGenerator, UserService userService) {
        this.jwtTokenProperties = jwtTokenProperties;
        this.jwtTokenGenerator = jwtTokenGenerator;
        this.userService = userService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
                csrf().disable()
                .headers().frameOptions().disable()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/api/group/**", "/api/group/**/**", "api/test/**", "api/test/**/**"
                ,"/secured/**", "secured/**/**/**").hasRole(IRoleConstants.USER)
                .antMatchers("/api/login/username", "/api/register").permitAll()
                .and()
                .addFilter(jwtAuthorizationFilter());
    }

    @Bean
    public JwtAuthorizationFilter jwtAuthorizationFilter() throws Exception {
        return new JwtAuthorizationFilter(authenticationManager(), jwtTokenGenerator, jwtTokenProperties, userService);
    }
}

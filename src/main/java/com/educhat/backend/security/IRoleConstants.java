package com.educhat.backend.security;

public interface IRoleConstants {
    String USER = "USER";
    String ADMIN = "ADMIN";
    String GUEST = "GUEST";

    String USER_ROLE_STRING = "ROLE_USER";
    String ADMIN_ROLE_STRING = "ROLE_ADMIN";
    String GUEST_ROLE_STRING = "ROLE_GUEST";
}

package com.educhat.backend.security.expression;

import com.educhat.backend.datamodel.CustomUserDetails;
import com.educhat.backend.datamodel.dto.UserDTO;
import com.educhat.backend.datamodel.request.ScoreTestCompletionRequest;
import com.educhat.backend.exception.NoPrivilegeException;
import com.educhat.backend.service.GroupMemberService;
import com.educhat.backend.util.IBusinessRoleConstants;
import lombok.Setter;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;

public class BusinessRoleMethodSecurityExpressionRoot extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {
    @Setter
    GroupMemberService groupMemberService;

    public BusinessRoleMethodSecurityExpressionRoot(Authentication authentication){
        super(authentication);
    }

    public final boolean canInviteToGroup(String groupId){
        return doesMemberHaveBusinessPrivilege(groupId, IBusinessRoleConstants.Privileges.INVITE_MEMBERS);
    }

    public final boolean canCreateStudentTest(String groupId){
        return doesMemberHaveBusinessPrivilege(groupId, IBusinessRoleConstants.Privileges.CREATE_TEST);
    }

    public final boolean canRetrieveActiveStudentTests(String groupId){
        return doesMemberHaveBusinessPrivilege(groupId, IBusinessRoleConstants.Privileges.RETRIEVE_TEST_ACTIVE);
    }
    public final boolean canCompleteTest(String groupId){
        return doesMemberHaveBusinessPrivilege(groupId, IBusinessRoleConstants.Privileges.COMPLETE_TEST);
    }

    public final boolean canScoreTest(String groupId, ScoreTestCompletionRequest request){
        if(request.getResult() != null && !doesMemberHaveBusinessPrivilege(groupId, IBusinessRoleConstants.Privileges.ADD_TEST_SCORE))
            throw new NoPrivilegeException(IBusinessRoleConstants.Privileges.ADD_TEST_SCORE);
        else if(request.getComment() != null && !doesMemberHaveBusinessPrivilege(groupId, IBusinessRoleConstants.Privileges.ADD_TEST_COMMENT))
            throw new NoPrivilegeException(IBusinessRoleConstants.Privileges.ADD_TEST_COMMENT);
        return true;
    }


    public final boolean canRetrieveTestCompletions(String groupId){
        //TODO ....
        return doesMemberHaveBusinessPrivilege(groupId, IBusinessRoleConstants.Privileges.READ_TEST_SCORE_ALL);
    }

    public final boolean canSendMessagesAll(String groupId){
        return doesMemberHaveBusinessPrivilege(groupId, IBusinessRoleConstants.Privileges.SEND_MESSAGE_ALL);
    }

    public final boolean canSendMessagesPrivate(String groupId){
        return doesMemberHaveBusinessPrivilege(groupId, IBusinessRoleConstants.Privileges.SEND_MESSAGE_PRIVATE);
    }

    public final boolean isMemberOf(String groupId){
        return groupMemberService.isUserMemberOfGroup(getUserFromAuthentication(), groupId);
    }

    private boolean doesMemberHaveBusinessPrivilege(String groupId, String businessRoleConstant){
        return groupMemberService.doesMemberHaveBusinessPrivilege(getUserFromAuthentication(), groupId, businessRoleConstant);
    }

    private UserDTO getUserFromAuthentication(){
        return ((CustomUserDetails)getAuthentication().getPrincipal()).getUser();
    }

    @Override
    public void setFilterObject(Object filterObject) {

    }

    @Override
    public Object getFilterObject() {
        return null;
    }

    @Override
    public void setReturnObject(Object returnObject) {

    }

    @Override
    public Object getReturnObject() {
        return null;
    }

    @Override
    public Object getThis() {
        return null;
    }
}

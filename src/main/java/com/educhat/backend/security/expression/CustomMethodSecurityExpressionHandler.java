package com.educhat.backend.security.expression;

import com.educhat.backend.service.GroupMemberService;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;

public class CustomMethodSecurityExpressionHandler extends DefaultMethodSecurityExpressionHandler {
    AuthenticationTrustResolver trustResolver = new AuthenticationTrustResolverImpl();
    GroupMemberService groupMemberService;

    public CustomMethodSecurityExpressionHandler(GroupMemberService groupMemberService) {
        super();
        this.groupMemberService = groupMemberService;
    }

    @Override
    protected MethodSecurityExpressionOperations createSecurityExpressionRoot(
            Authentication authentication, MethodInvocation invocation) {
        BusinessRoleMethodSecurityExpressionRoot root =
                new BusinessRoleMethodSecurityExpressionRoot(authentication);
        root.setPermissionEvaluator(getPermissionEvaluator());
        root.setTrustResolver(this.trustResolver);
        root.setRoleHierarchy(getRoleHierarchy());
        root.setGroupMemberService(groupMemberService);
        return root;
    }
}

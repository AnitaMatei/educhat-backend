package com.educhat.backend.security;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SecurityHelper {
    /*
    At least one digit, one lowercase, one uppercase, one special character, length between 8 and 16.
     */
    private static final String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,16}$";
    private static final Pattern pattern = Pattern.compile(PASSWORD_REGEX);

    public static boolean isPasswordvalid(String password){
        return pattern.matcher(password).matches();
    }
}

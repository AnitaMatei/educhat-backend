package com.educhat.backend.repository;

import com.educhat.backend.datamodel.entity.SecurityRole;
import com.educhat.backend.datamodel.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface SecurityRoleRepository extends JpaRepository<SecurityRole, Long> {
    public Collection<SecurityRole> findAllByUsersWithRole(User user);
    public SecurityRole findByRoleName(String roleName);
}

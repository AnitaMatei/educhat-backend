package com.educhat.backend.repository;

import com.educhat.backend.datamodel.entity.Group;
import com.educhat.backend.datamodel.entity.GroupMember;
import com.educhat.backend.datamodel.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GroupMemberRepository extends JpaRepository<GroupMember, Long> {
    public Optional<GroupMember> findByAssociatedUser_UsernameAndGroup_BusinessId(String username, String groupBusinessId);
}

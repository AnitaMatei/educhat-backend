package com.educhat.backend.repository;

import com.educhat.backend.datamodel.dto.GroupInviteDTO;
import com.educhat.backend.datamodel.entity.GroupInvite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GroupInviteRepository extends JpaRepository<GroupInvite, Long> {
    public Optional<GroupInvite> findByBusinessId(String businessId);
}

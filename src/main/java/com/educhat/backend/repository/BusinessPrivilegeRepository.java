package com.educhat.backend.repository;

import com.educhat.backend.datamodel.entity.BusinessPrivilege;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusinessPrivilegeRepository extends JpaRepository<BusinessPrivilege, Long> {
}

package com.educhat.backend.repository;

import com.educhat.backend.datamodel.entity.StudentTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentTestRepository extends JpaRepository<StudentTest, Long> {
    public Optional<StudentTest> findByBusinessId(String businessId);
}

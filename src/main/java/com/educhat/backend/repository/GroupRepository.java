package com.educhat.backend.repository;

import com.educhat.backend.datamodel.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GroupRepository extends JpaRepository<Group,Long> {
    public Optional<Group> findByBusinessId(String businessId);
}

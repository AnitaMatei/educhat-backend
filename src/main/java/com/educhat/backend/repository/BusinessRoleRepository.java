package com.educhat.backend.repository;

import com.educhat.backend.datamodel.entity.BusinessRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BusinessRoleRepository extends JpaRepository<BusinessRole, Long> {
    public Optional<BusinessRole> findByRoleName(String roleName);
}

package com.educhat.backend.repository;

import com.educhat.backend.datamodel.entity.TestCompletion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface TestCompletionRepository extends JpaRepository<TestCompletion, Long> {
    public Optional<TestCompletion> findByAssociatedTest_BusinessIdAndCompletedBy_Id(String businessId, Long memberId);
    public Optional<TestCompletion> findByAssociatedTest_BusinessIdAndAndCompletedBy_Group_BusinessId(String testId, String groupId);
    public Collection<TestCompletion> findAllByAssociatedTest_BusinessIdAndAssociatedTest_CreatedBy_Group_BusinessId(String testId, String groupId);
}

package com.educhat.backend.service;

import com.educhat.backend.config.ApplicationConfig;
import com.educhat.backend.datamodel.dto.StudentTestDTO;
import com.educhat.backend.datamodel.dto.UserDTO;
import com.educhat.backend.datamodel.entity.Group;
import com.educhat.backend.datamodel.entity.GroupMember;
import com.educhat.backend.datamodel.entity.StudentTest;
import com.educhat.backend.datamodel.entity.User;
import com.educhat.backend.datamodel.request.StudentTestCreateRequest;
import com.educhat.backend.exception.ResourceNoLongerExistingException;
import com.educhat.backend.repository.StudentTestRepository;
import com.educhat.backend.repository.TestCompletionRepository;
import org.assertj.core.util.Sets;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import javax.persistence.criteria.CriteriaBuilder;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@ContextConfiguration(classes = ApplicationConfig.class)
class StudentTestServiceTest {
    @Mock
    StudentTestRepository studentTestRepository;
    @Mock
    TestCompletionRepository testCompletionRepository;
    @Mock
    UserService userService;
    @Mock
    GroupMemberService groupMemberService;
    @Mock
    GroupService groupService;
    @Mock
    ModelMapper modelMapper;

    @InjectMocks
    StudentTestService studentTestService = new StudentTestService(studentTestRepository, testCompletionRepository,
            userService, groupMemberService, groupService, modelMapper);


    @Test
    void test_createStudentTest_returnsStudentTestDTO() {
        UserDTO userDTO = new UserDTO();
        StudentTestCreateRequest request = new StudentTestCreateRequest();
        String groupId = "test id";
        GroupMember groupMember = new GroupMember();
        StudentTest studentTest = new StudentTest();
        StudentTestDTO studentTestDTO = new StudentTestDTO();
        studentTestDTO.setTestName("test name");
        studentTestDTO.setParseableString("test string");
        studentTestDTO.setDeadline(Timestamp.from(Instant.now()));
        studentTestDTO.setHardDeadline(true);
        studentTestDTO.setCreatedBy(userDTO);


        when(groupMemberService.getMembershipOfInGroup(userDTO, groupId)).thenReturn(groupMember);
        when(studentTestRepository.save(any(StudentTest.class))).thenReturn(studentTest);
        when(modelMapper.map(studentTest, StudentTestDTO.class)).thenReturn(studentTestDTO);

        StudentTestDTO result = studentTestService.createStudentTest(userDTO, request, groupId);

        assertEquals(studentTestDTO.getTestName(), result.getTestName());
        assertEquals(studentTestDTO.getParseableString(), result.getParseableString());
        assertEquals(studentTestDTO.getDeadline(), result.getDeadline());
        assertEquals(studentTestDTO.getHardDeadline(), result.getHardDeadline());
        assertEquals(studentTestDTO.getCreatedBy(), result.getCreatedBy());
    }

    @Test
    void test_isStudentTestAvailableToComplete() {
        String testId = "test id";
        StudentTest studentTest = new StudentTest();
        studentTest.setAvailable(true);
        studentTest.setDeadline(Timestamp.from(Instant.MAX));
        studentTest.setHardDeadline(false);

        when(studentTestRepository.findByBusinessId(testId)).thenReturn(java.util.Optional.of(studentTest));
        assertTrue(studentTestService.isTestAvailableToComplete(testId));

        studentTest.setAvailable(false);
        when(studentTestRepository.findByBusinessId(testId)).thenReturn(java.util.Optional.of(studentTest));
        assertFalse(studentTestService.isTestAvailableToComplete(testId));
        studentTest.setAvailable(true);
        studentTest.setDeadline(Timestamp.from(Instant.MIN));
        studentTest.setHardDeadline(true);
        when(studentTestRepository.findByBusinessId(testId)).thenReturn(java.util.Optional.of(studentTest));
        assertFalse(studentTestService.isTestAvailableToComplete(testId));
    }

    @Test
    void test_getAvailableTestFromGroup_returnsAvailableCount() {
        Group group = new Group();
        String groupId = "test id";
        group.setBusinessId(groupId);
        GroupMember gr1 = new GroupMember();
        GroupMember gr2 = new GroupMember();
        group.setGroupMembers(Sets.newLinkedHashSet(gr1, gr2));
        User user1 = new User();
        User user2 = new User();
        StudentTest studentTest = new StudentTest();
        studentTest.setAvailable(true);
        studentTest.setCreatedBy(gr1);
        studentTest.setDeadline(Timestamp.from(Instant.MAX));
        studentTest.setHardDeadline(false);
        StudentTest studentTest2 = new StudentTest();
        studentTest2.setAvailable(false);
        studentTest2.setCreatedBy(gr2);
        studentTest2.setDeadline(Timestamp.from(Instant.MAX));
        studentTest2.setHardDeadline(false);
        StudentTest studentTest3 = new StudentTest();
        studentTest3.setAvailable(true);
        studentTest3.setCreatedBy(gr1);
        studentTest3.setDeadline(Timestamp.from(Instant.MAX));
        studentTest3.setHardDeadline(false);
        StudentTest studentTest4 = new StudentTest();
        studentTest4.setAvailable(false);
        studentTest4.setCreatedBy(gr2);
        studentTest4.setDeadline(Timestamp.from(Instant.MAX));
        studentTest4.setHardDeadline(false);
        gr1.setCreatedTests(Sets.newLinkedHashSet(studentTest, studentTest2));
        gr2.setCreatedTests(Sets.newLinkedHashSet(studentTest3, studentTest4));
        StudentTestDTO test1 = new StudentTestDTO();
        StudentTestDTO test2 = new StudentTestDTO();
        StudentTestDTO test3 = new StudentTestDTO();
        StudentTestDTO test4 = new StudentTestDTO();
        UserDTO userDTO1 = new UserDTO();
        UserDTO userDTO2 = new UserDTO();
        test1.setCreatedBy(userDTO1);
        test2.setCreatedBy(userDTO1);
        test3.setCreatedBy(userDTO2);
        test4.setCreatedBy(userDTO2);

        when(groupService.getGroupByBusinessIdInternal(groupId)).thenReturn(group);
        when(modelMapper.map(studentTest, StudentTestDTO.class)).thenReturn(test1);
        when(modelMapper.map(studentTest2, StudentTestDTO.class)).thenReturn(test2);
        when(modelMapper.map(studentTest3, StudentTestDTO.class)).thenReturn(test3);
        when(modelMapper.map(studentTest4, StudentTestDTO.class)).thenReturn(test4);
        when(modelMapper.map(user1, UserDTO.class)).thenReturn(userDTO1);
        when(modelMapper.map(user2, UserDTO.class)).thenReturn(userDTO2);

        assertEquals(2, studentTestService.getAvailableTestFromGroup(groupId).size());

    }

    @Test
    void test_getStudentTestById() {
        String testId = "test id";

        when(studentTestRepository.findByBusinessId(testId)).thenReturn(Optional.of(new StudentTest()));
        when(studentTestRepository.findByBusinessId("zzz")).thenReturn(Optional.empty());
        assertNotNull(studentTestService.getStudentTestById(testId));
        assertThrows(ResourceNoLongerExistingException.class, () -> studentTestService.getStudentTestById("zzz"));
    }
}

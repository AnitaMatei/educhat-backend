package com.educhat.backend.service;

import com.educhat.backend.config.ApplicationConfig;
import com.educhat.backend.datamodel.dto.UserDTO;
import com.educhat.backend.datamodel.entity.User;
import com.educhat.backend.datamodel.request.LoginRequest;
import com.educhat.backend.datamodel.request.LoginResponse;
import com.educhat.backend.datamodel.request.RegisterRequest;
import com.educhat.backend.exception.AlreadyExistingAccountException;
import com.educhat.backend.exception.InvalidLoginDetailsException;
import com.educhat.backend.exception.InvalidRegisterDetailsException;
import com.educhat.backend.repository.UserRepository;
import com.educhat.backend.security.jwt.JwtTokenGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@ContextConfiguration(classes = ApplicationConfig.class)
public class AuthenticationServiceTest {
    @Mock
    UserRepository userRepository;
    @Mock
    UserService userService;
    @Mock
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Mock
    JwtTokenGenerator jwtTokenGenerator;

    @InjectMocks
    AuthenticationService authenticationService = new AuthenticationService(userRepository, userService,
            bCryptPasswordEncoder, jwtTokenGenerator);

    @Test
    void test_registerWithUsernameAndPassword_returnsLoginResponse() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsername("matei");
        registerRequest.setPassword("Cicoare2@");
        registerRequest.setEmail("email");
        registerRequest.setFirstName("firstname");
        registerRequest.setLastName("lastname");
        String jwtToken = "daaaa";

        when(userService.createNewUser(registerRequest)).thenReturn(new UserDTO());
        when(jwtTokenGenerator.generateTokenWithPrefix(any(UserDTO.class))).thenReturn(jwtToken);

        when(userRepository.findByUsernameOrEmail(registerRequest.getUsername(), registerRequest.getEmail())).thenReturn(Optional.empty());
        assertEquals(new LoginResponse(jwtToken),
                authenticationService.registerWithUsernameAndPassword(registerRequest));

        registerRequest.setPassword("cicoare2@");
        assertThrows(InvalidRegisterDetailsException.class,
                () -> authenticationService.registerWithUsernameAndPassword(registerRequest));
        registerRequest.setPassword("Cicoare2@");
        registerRequest.setUsername(null);
        assertThrows(InvalidRegisterDetailsException.class,
                () -> authenticationService.registerWithUsernameAndPassword(registerRequest));

        registerRequest.setUsername("matei");
        when(userRepository.findByUsernameOrEmail(registerRequest.getUsername(), registerRequest.getEmail())).thenReturn(Optional.of(new User()));
        assertThrows(AlreadyExistingAccountException.class,
                () -> authenticationService.registerWithUsernameAndPassword(registerRequest));
    }

    @Test
    void test_loginWithUsernameAndPassword_returnsLoginResponse() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("matei");
        loginRequest.setPassword("Cicoare2@");
        String jwtToken = "daaaa";
        User user = new User();
        user.setPassword("Cicoare2@");

        when(userRepository.findByUsername(loginRequest.getUsername())).thenReturn(Optional.of(user));
        when(userService.findByUsername(loginRequest.getUsername())).thenReturn(new UserDTO());
        when(bCryptPasswordEncoder.matches(loginRequest.getPassword(), user.getPassword())).thenReturn(true);
        when(jwtTokenGenerator.generateTokenWithPrefix(any(UserDTO.class))).thenReturn(jwtToken);

        assertEquals(new LoginResponse(jwtToken), authenticationService.loginWithUsernameAndPassword(loginRequest));

        user.setPassword("wrong");
        assertThrows(InvalidLoginDetailsException.class,
                () -> authenticationService.loginWithUsernameAndPassword(loginRequest));

        user.setPassword("Cicoare2@");
        when(userRepository.findByUsername(loginRequest.getUsername())).thenReturn(Optional.empty());
        assertThrows(InvalidLoginDetailsException.class,
                () -> authenticationService.loginWithUsernameAndPassword(loginRequest));


    }
}

package com.educhat.backend.service;

import com.educhat.backend.config.ApplicationConfig;
import com.educhat.backend.datamodel.dto.GroupDTO;
import com.educhat.backend.datamodel.dto.GroupMemberDTO;
import com.educhat.backend.datamodel.dto.UserDTO;
import com.educhat.backend.datamodel.entity.*;
import com.educhat.backend.datamodel.request.GroupCreateRequest;
import com.educhat.backend.exception.ResourceNoLongerExistingException;
import com.educhat.backend.repository.BusinessRoleRepository;
import com.educhat.backend.repository.GroupRepository;
import com.educhat.backend.util.IBusinessRoleConstants;
import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.util.collections.Sets;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

@SpringBootTest
@ContextConfiguration(classes = ApplicationConfig.class)
class GroupServiceTest {
    @Mock
    GroupRepository groupRepository;
    @Mock
    UserService userService;
    @Mock
    BusinessRoleRepository businessRoleRepository;
    @Mock
    GroupMemberService groupMemberService;
    @Mock
    ModelMapper modelMapper;
    @InjectMocks
    GroupService groupService = new GroupService(groupRepository, userService, businessRoleRepository,
            groupMemberService, modelMapper);

    @Test
    void test_createGroup_returnsDTO() {
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail("test@test.test");
        userDTO.setUsername("test");
        GroupCreateRequest request = new GroupCreateRequest();
        request.setGroupName("test group");
        request.setPictureUrl("test picture");
        User user = new User();
        Group group = new Group();
        BusinessRole businessRole = new BusinessRole();
        GroupDTO groupDTO = new GroupDTO();
        groupDTO.setGroupName(request.getGroupName());
        groupDTO.setPictureUrl(request.getPictureUrl());
        groupDTO.setMembers(Sets.newSet(new GroupMemberDTO()));

        when(userService.findUserByEmailOrUsernameInternal(userDTO)).thenReturn(user);
        when(businessRoleRepository.findByRoleName(IBusinessRoleConstants.TEACHER)).thenReturn(java.util.Optional.of(businessRole));
        when(groupRepository.save(any(Group.class))).thenReturn(group);
        when(modelMapper.map(group, GroupDTO.class)).thenReturn(groupDTO);

        GroupDTO result = groupService.createGroup(userDTO, request);
        assertEquals(userDTO, result.getCreatedBy());
        assertEquals(Sets.newSet(new GroupMemberDTO()), result.getMembers());
        assertEquals(userDTO.getEmail(), result.getCreatedBy().getEmail());
        assertEquals(userDTO.getUsername(), result.getCreatedBy().getUsername());
        assertEquals(request.getGroupName(), request.getGroupName());
        assertEquals(request.getPictureUrl(), request.getPictureUrl());
    }

    @Test
    void test_getGroupByBusinessId_returnsDTO() {
        String groupId = "test id";
        Group group = new Group();
        User user = new User();
        GroupMember groupMember = new GroupMember();
        group.setBusinessId(groupId);
        group.setCreatedBy(user);
        group.setGroupMembers(Sets.newSet(groupMember));
        GroupDTO groupDTO = new GroupDTO();
        UserDTO userDTO = new UserDTO();
        GroupMemberDTO groupMemberDTO = new GroupMemberDTO();
        groupDTO.setBusinessId(groupId);
        groupDTO.setCreatedBy(userDTO);
        groupDTO.setMembers(Sets.newSet(groupMemberDTO));

        when(groupRepository.findByBusinessId(groupId)).thenReturn(Optional.of(group));
        when(modelMapper.map(group, GroupDTO.class)).thenReturn(groupDTO);
        when(modelMapper.map(user, UserDTO.class)).thenReturn(userDTO);
        when(groupMemberService.getGroupMembersOf(group)).thenReturn(Sets.newSet(groupMemberDTO));

        GroupDTO result = groupService.getGroupByBusinessId(groupId);
        assertEquals(groupId, result.getBusinessId());
        assertEquals(userDTO, result.getCreatedBy());
        assertEquals(Sets.newSet(groupMemberDTO), result.getMembers());
    }

    @Test
    void test_getGroupByBusinessIdInternal() {
        String groupId = "test id";
        Group group = new Group();

        when(groupRepository.findByBusinessId(groupId)).thenReturn(Optional.of(group));
        assertEquals(group, groupService.getGroupByBusinessIdInternal(groupId));

        when(groupRepository.findByBusinessId(groupId)).thenReturn(Optional.empty());
        assertThrows(ResourceNoLongerExistingException.class, () -> groupService.getGroupByBusinessIdInternal(groupId));
    }

}

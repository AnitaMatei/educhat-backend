package com.educhat.backend.service;

import com.educhat.backend.config.ApplicationConfig;
import com.educhat.backend.datamodel.dto.UserDTO;
import com.educhat.backend.datamodel.entity.*;
import com.educhat.backend.exception.ResourceNoLongerExistingException;
import com.educhat.backend.repository.GroupMemberRepository;
import com.educhat.backend.repository.GroupRepository;
import com.educhat.backend.repository.UserRepository;
import com.educhat.backend.util.IBusinessRoleConstants;
import org.assertj.core.util.Sets;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ContextConfiguration(classes = ApplicationConfig.class)
public class GroupMemberServiceTest {
    @Mock
    GroupMemberRepository groupMemberRepository;
    @Mock
    UserRepository userRepository;
    @Mock
    GroupRepository groupRepository;
    @Mock
    ModelMapper modelMapper;

    @InjectMocks
    GroupMemberService groupMemberService = new GroupMemberService(groupMemberRepository, userRepository,
            groupRepository, modelMapper);


    @Test
    void test_doesMemberHaveBusinessPrivilege() {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("test username");
        String groupId = "test id";
        GroupMember groupMember = new GroupMember();
        Group group = new Group();
        group.setBusinessId(groupId);
        User user = new User();
        user.setGroupMemberships(Sets.newLinkedHashSet(groupMember));
        GroupMemberBusinessRole grRole = new GroupMemberBusinessRole();
        BusinessRole businessRole = new BusinessRole();
        BusinessPrivilege businessPrivilege = new BusinessPrivilege();
        businessPrivilege.setPrivilegeName(IBusinessRoleConstants.Privileges.ADD_TEST_COMMENT);
        businessRole.setPrivileges(Sets.newLinkedHashSet(businessPrivilege));
        grRole.setRole(businessRole);
        groupMember.setGroupRole(grRole);
        groupMember.setGroup(group);

        when(userRepository.findByUsername("test username")).thenReturn(Optional.of(user));

        assertTrue(groupMemberService.doesMemberHaveBusinessPrivilege(userDTO, groupId,
                IBusinessRoleConstants.Privileges.ADD_TEST_COMMENT));
        assertFalse(groupMemberService.doesMemberHaveBusinessPrivilege(userDTO, groupId,
                IBusinessRoleConstants.Privileges.CREATE_TEST));
    }

    @Test
    void test_isUserMemberOfGroup() {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("test username");
        String groupId = "test id";
        GroupMember groupMember = new GroupMember();
        Group group = new Group();
        group.setBusinessId(groupId);
        groupMember.setGroup(group);
        User user = new User();
        user.setGroupMemberships(Sets.newLinkedHashSet(groupMember));
        Group group2 = new Group();
        String groupId2 = "test id 2";
        group2.setBusinessId(groupId2);

        when(userRepository.findByUsername("test username")).thenReturn(Optional.of(user));

        assertTrue(groupMemberService.isUserMemberOfGroup(userDTO, groupId));
        assertFalse(groupMemberService.isUserMemberOfGroup(userDTO, groupId2));
    }

    @Test
    void test_getMembershipOfInGroup_returnsGroupMember() {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("test username");
        String groupId = "test id";
        GroupMember groupMember = new GroupMember();
        Group group = new Group();
        group.setBusinessId(groupId);
        groupMember.setGroup(group);
        User user = new User();
        user.setGroupMemberships(Sets.newLinkedHashSet(groupMember));
        Group group2 = new Group();
        String groupId2 = "test id 2";
        group2.setBusinessId(groupId2);

        when(groupMemberRepository.findByAssociatedUser_UsernameAndGroup_BusinessId("test username", groupId)).thenReturn(Optional.of(groupMember));
        when(groupMemberRepository.findByAssociatedUser_UsernameAndGroup_BusinessId("test username", groupId2)).thenReturn(Optional.empty());


        assertNotNull(groupMemberService.getMembershipOfInGroup(userDTO, groupId));
        assertThrows(ResourceNoLongerExistingException.class, () -> groupMemberService.getMembershipOfInGroup(userDTO
                , groupId2));
    }
}
